<x-app-layout>

</x-app-layout>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    @include("admin.admincss")

</head>

<body>
    <div class="container-scroller">
        @include("admin.navbar")



        <div class="container" style="position: relative; top: 60px;">
            <h1 style="text-align: center">Customer Orders</h1><br>
            <form class="example" action="get" action="{{ url('/search') }}">
                @csrf
                <input type="text" placeholder="Search.." name="search">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form><br><br>
            <table id="food" style="width:70em; ">
                <tr>
                    <th>Name</th>
                    <th>Foodname</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Total Price</th>
                </tr>
                @foreach ($data as $data)
                    <tr>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->foodname }}</td>
                        <td>{{ $data->phone }}</td>
                        <td>{{ $data->addrese }}</td>
                        <td>${{ $data->price }}</td>
                        <td>{{ $data->quantity }}</td>
                        <td>${{ $data->price * $data->quantity }}</td>
                        {{-- <td>
                            <a href="{{ url('/deletereservation', $data->id) }}"><i class="fas fa-trash"
                                    style="color: red"></i></a>
                        </td> --}}
                    </tr>
                @endforeach

            </table>
        </div>

    </div>
    @include("admin.adminscript")
</body>

</html>
