<x-app-layout>

</x-app-layout>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    @include("admin.admincss")

</head>

<body>
    <div class="container-scroller">
        @include("admin.navbar")

        <div class="container" style="position: relative; top: 60px;">
            <form action="{{ url('/uploadfood') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-1">
                        <label>Title</label>
                    </div>
                    <div class="col-6">
                        <input type="text" name="title" placeholder="Write title..." required>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-1">
                        <label>Price</label>
                    </div>
                    <div class="col-6">
                        <input type="num" name="price" placeholder="Price" required>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-1">
                        <label>Image</label>
                    </div>
                    <div class="col-75">
                        <input type="file" name="image" placeholder="Upload Image" required>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-1">
                        <label>Description</label>
                    </div>
                    <div class="col-6">
                        <input type="text" name="description" placeholder="Description" required>
                    </div>
                </div>

                <br>
                <div style="display: flex; justify-content: start;">
                    <input style="background-color: green;" type="submit" value="Save">
                </div>
            </form><br><br><br>

            <div>
                <table id="food" style="width:47em;">
                    <tr>
                        <th>Food Menu</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                    @foreach ($data as $data)
                        <tr>
                            <td>{{ $data->title }}</td>
                            <td>{{ $data->price }}</td>
                            <td>{{ $data->description }}</td>
                            <td><img src="/foodimage/{{ $data->image }}" alt="" style="width:40px;height: 40px;"></td>
                            <td>
                                <a href="{{ url('/deletemenu', $data->id) }}"><i class="fas fa-trash"
                                        style="color: red"></i></a>
                                &nbsp;|&nbsp;
                                <a href="{{ url('/updateview', $data->id) }}"><i class="fas fa-edit"
                                        style="color: green"></i></a>
                            </td>
                        </tr>
                    @endforeach

                </table>
            </div>

        </div>



    </div>
    @include("admin.adminscript")
</body>

</html>
