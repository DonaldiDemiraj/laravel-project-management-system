<x-app-layout>

</x-app-layout>

<!DOCTYPE html>
<html lang="en">

<head>
    <base href="/public">
    <!-- Required meta tags -->
    @include("admin.admincss")

</head>

<body>
    <div class="container-scroller">
        @include("admin.navbar")
        <div class="container" style="position: relative; top: 60px;">
            <form action="{{ url('/update', $data->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-1">
                        <label>Title</label>
                    </div>
                    <div class="col-6">
                        <input type="text" name="title" value="{{ $data->title }}" required>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-1">
                        <label>Price</label>
                    </div>
                    <div class="col-6">
                        <input type="num" name="price" value="{{ $data->price }}" required>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-1">
                        <label>Description</label>
                    </div>
                    <div class="col-6">
                        <input type="text" name="description" value="{{ $data->description }}" required>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-1">
                        <label>Old Image</label>
                    </div>
                    <div class="col-75">
                        <img height="40px;" width="300px;" src="/foodimage/{{ $data->image }}" alt="">
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-1">
                        <label>New Image</label>
                    </div>
                    <div class="col-75">
                        <input type="file" name="image" placeholder="Upload Image" required>
                    </div>
                </div><br>
                <div style="display: flex; justify-content: start;">
                    <input style="background-color: green;" type="submit" value="Save">
                </div>
            </form>
        </div>
    </div>
    @include("admin.adminscript")
</body>

</html>
