<x-app-layout>

</x-app-layout>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    @include("admin.admincss")

</head>

<body>
    <div class="container-scroller">
        @include("admin.navbar")

        <div class="container" style="position: relative; top: 60px;">
            <table id="food" style="width:70em; ">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Date</th>
                    <th>Numer of Guest</th>
                    <th>Time</th>
                    <th>Message</th>
                    <th>Action</th>
                </tr>
                @foreach ($data as $data)
                    <tr>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->phone }}</td>
                        <td>{{ $data->date }}</td>
                        <td>{{ $data->guest }}</td>
                        <td>{{ $data->time }}</td>
                        <td>{{ $data->message }}</td>
                        <td>
                            <a href="{{ url('/deletereservation', $data->id) }}"><i class="fas fa-trash"
                                    style="color: red"></i></a>
                        </td>
                    </tr>
                @endforeach

            </table>
        </div>
    </div>
    @include("admin.adminscript")
</body>

</html>
