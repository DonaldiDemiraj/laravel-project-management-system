<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

</head>

<body>
    <x-app-layout>

    </x-app-layout>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        @include("admin.admincss")

    </head>



    <body>
        <div class="container-scroller">
            @include("admin.navbar")

            <div style="position: relative; top: 60px; right: -60px;">

                <table id="food" style="width:70em;">
                    <tr>
                        <th style="text-align: center;">Name</th>
                        <th style="text-align: center;">Email</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                    @foreach ($data as $data)
                        <tr>
                            <td style="text-align: center;">{{ $data->name }}</td>
                            <td style="text-align: center;">{{ $data->email }}</td>
                            @if ($data->usertype == '0')
                                <td style="text-align: center;"><a href="{{ url('/deleteuser', $data->id) }}"
                                        style="color: red; text-decoration: none;"><i class="fas fa-trash"></i>
                                        &nbsp; </a></td>
                            @else
                                <td style="text-align: center;"><a>Not Allowed</a></td>
                            @endif
                        </tr>
                    @endforeach

                </table>
            </div>


        </div>
        @include("admin.adminscript")
    </body>

    </html>
</body>

</html>
