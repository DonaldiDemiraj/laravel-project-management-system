<x-app-layout>

</x-app-layout>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    @include("admin.admincss")

</head>

<body>
    <div class="container-scroller">
        @include("admin.navbar")

        <div class="container" style="position: relative; top: 60px; margin-left: 8em;">
            <form action="{{ url('/uploadchef') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-1">
                        <label>Name</label>
                    </div>
                    <div class="col-6">
                        <input type="text" name="name" placeholder="Chef Name" required>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-1">
                        <label>Speciality</label>
                    </div>
                    <div class="col-6">
                        <input type="text" name="speciality" placeholder="Spqciality" required>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-1">
                        <label>Image</label>
                    </div>
                    <div class="col-75">
                        <input type="file" name="image" placeholder="Upload Image" required>
                    </div>
                </div><br>

                <br>
                <div style="display: flex; justify-content: start;">
                    <input style="background-color: green;" type="submit" value="Save">
                </div>
            </form>
        </div>
        <div class="container" style="position: relative; top: 60px; margin-left: 5em;">
            <table id="food" style="width:47em;">
                <tr>
                    <th>Chef Name</th>
                    <th>Speciality</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                @foreach ($data as $data)
                    <tr>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->speciality }}</td>
                        <td><img src="/chefimage/{{ $data->image }}" alt="" style="width:40px;height: 40px;"></td>
                        <td>
                            <a href="{{ url('/deletechef', $data->id) }}"><i class="fas fa-trash"
                                    style="color: red"></i></a>
                            &nbsp;|&nbsp;
                            <a href="{{ url('/updatechef', $data->id) }}"><i class="fas fa-edit"
                                    style="color: green"></i></a>
                        </td>
                    </tr>
                @endforeach

            </table>
        </div>
    </div>
    @include("admin.adminscript")
</body>

</html>
