<x-app-layout>

</x-app-layout>

<!DOCTYPE html>
<html lang="en">

<head>
    <base href="/public">
    <!-- Required meta tags -->
    @include("admin.admincss")

</head>

<body>
    <div class="container-scroller">
        @include("admin.navbar")

        <div class="container" style="position: relative; top: 60px; margin-left: 8em;">
            <form action="{{ url('/updatefoodchef', $data->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-1">
                        <label>Name</label>
                    </div>
                    <div class="col-6">
                        <input type="text" name="name" value="{{ $data->name }}" required>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-1">
                        <label>Speciality</label>
                    </div>
                    <div class="col-6">
                        <input type="text" name="speciality" value="{{ $data->speciality }}" required>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-1">
                        <label>Old Image</label>
                    </div>
                    <div class="col-75">
                        <img width="100" src="/chefimage/{{ $data->image }}" alt="">
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-1">
                        <label>Image</label>
                    </div>
                    <div class="col-75">
                        <input type="file" name="image" required>
                    </div>
                </div><br>

                <br>
                <div style="display: flex; justify-content: start;">
                    <input style="background-color: green;" type="submit" value="Save">
                </div>
            </form>
        </div>
    </div>
    @include("admin.adminscript")
</body>

</html>
